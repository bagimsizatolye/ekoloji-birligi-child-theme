<?php
/**
 * Theme functions file
 *
 * Contains all of the Theme's setup functions, custom functions,
 * custom hooks and Theme settings.
 *
 * @package accesspress-mag-child
 * @license license.txt
 * @since   1.0
 *
 */

add_action( 'after_setup_theme', 'ba_load_childtheme_languages', 5 );
function ba_load_childtheme_languages() {
    /* this theme supports localization */
    load_child_theme_textdomain( 'accesspress-mag', get_stylesheet_directory() . '/languages' );
}

/* Adds the child theme setup function to the 'after_setup_theme' hook. */
add_action( 'after_setup_theme', 'ba_child_theme_setup', 11 );

/**
 * Setup function. All child themes should run their setup within this function. The idea is to add/remove
 * filters and actions after the parent theme has been set up. This function provides you that opportunity.
 *
 * @since 1.0
 */
function ba_child_theme_setup() {



}
